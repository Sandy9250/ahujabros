package com.ahujaBros.receiver;

public interface ConnectivityReceiveListener {
    void onNetworkConnectionChanged(boolean isConnected);
}
