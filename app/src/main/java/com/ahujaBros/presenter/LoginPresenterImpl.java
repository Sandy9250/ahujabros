package com.ahujaBros.presenter;

import com.ahujaBros.api.ApiClient;
import com.ahujaBros.model.Login.LoginResponse;
import com.ahujaBros.model.home.HomeResponse;
import com.ahujaBros.ui.LoginView;
import com.ahujaBros.ui.activity.LoginActivity;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by insonix on 31/5/17.
 */

public class LoginPresenterImpl implements LoginPresenter {
    LoginView mView;
    private ApiClient mApiClient;


    public LoginPresenterImpl(LoginView view) {
        mView = view;
        mApiClient=new ApiClient();
    }

    @Override
    public void loginUser(String userName, String pwd, String id, String type, String token) {
        mView.showProgress();
        Observable<LoginResponse> categoriesCall = mApiClient.getInstance().Login(userName, pwd, id, type, token);
        categoriesCall.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<LoginResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(LoginResponse loginResponse) {
                        mView.hideProgress();
                        mView.onSuccessLogin(loginResponse);
                    }
                });
    }
}
