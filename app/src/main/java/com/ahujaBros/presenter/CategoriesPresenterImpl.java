package com.ahujaBros.presenter;

import android.app.Activity;
import android.util.Log;

import com.ahujaBros.api.ApiClient;
import com.ahujaBros.model.Search.ProductFoundResponse;
import com.ahujaBros.model.Search.SearchKeywordResponse;
import com.ahujaBros.model.generic.BadgeResponse;
import com.ahujaBros.model.menu.MenuResponse;
import com.ahujaBros.ui.MainView;
import com.ahujaBros.utils.MyPreferences;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static android.content.ContentValues.TAG;

/**
 * Created by insonix on 18/5/17.
 */

public class CategoriesPresenterImpl implements CategoriesPresenter {

    Activity ctxt;
    ApiClient mApiClient;
    MainView mView;

    public CategoriesPresenterImpl(Activity activity,MainView view) {
        this.ctxt = activity;
        this.mApiClient=new ApiClient();
        mView=view;
    }

    @Override
    public void getCategories() {
        Observable<MenuResponse> categoriesCall = mApiClient.getInstance().fetchCategoriesList();
        categoriesCall.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<MenuResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(MenuResponse categoryResponse) {
                       mView.getMenuCategories(categoryResponse);
                    }
                });
    }

    @Override
    public void getBadgeCount() {
        Observable<BadgeResponse> categoriesCall = mApiClient.getInstance().getBadgeCount(MyPreferences.getInstance().getString(MyPreferences.Key.USERID));
        categoriesCall.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<BadgeResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(BadgeResponse response) {
                        mView.updateBadgeCount(response);
                    }
                });
    }


    @Override
    public void getSearchKeywords() {
        Observable<SearchKeywordResponse> searchListCall = mApiClient.getInstance().getKeywords();
        searchListCall.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<SearchKeywordResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(SearchKeywordResponse response) {
//                        mView.refreshData(response);
                        mView.searchKeywordsList(response.getData());
                    }
                });
    }

    @Override
    public void searchKeyword(String search) {
        mView.showProgress();
        Observable<ProductFoundResponse> searchListCall = mApiClient.getInstance().searchKeywords(search, String.valueOf(0));
        searchListCall.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ProductFoundResponse>() {
                    @Override
                    public void onCompleted() {
                        Log.i(TAG, "onCompleted: ");
                        mView.hideProgress();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.i(TAG, "onError: ");
                        mView.hideProgress();
                        mView.showError("No Products Found");
                    }

                    @Override
                    public void onNext(ProductFoundResponse response) {
                        Log.i(TAG, "onNext: ");
                        mView.hideProgress();
                        mView.searchedData(response);
                    }
                });
    }

}
