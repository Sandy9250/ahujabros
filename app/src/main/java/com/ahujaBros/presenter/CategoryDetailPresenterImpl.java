package com.ahujaBros.presenter;

import android.content.Context;
import android.util.Log;

import com.ahujaBros.api.ApiClient;
import com.ahujaBros.model.AddToCart.AddToCartResponse;
import com.ahujaBros.model.CategoryDetail.CategoryDetailResponse;
import com.ahujaBros.model.CategoryDetail.ProductsItem;
import com.ahujaBros.ui.CategoryView;
import com.ahujaBros.utils.MyPreferences;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static android.content.ContentValues.TAG;

/**
 * Created by insonix on 25/5/17.
 */

public class CategoryDetailPresenterImpl implements CategoryDetailPresenter {

    Context ctx;
    CategoryView mView;
    private ApiClient mApiClient;
    private List sampleList=new ArrayList<>();
    private ArrayList<String> supplierList;

    public CategoryDetailPresenterImpl(Context ctx, CategoryView mView) {
        this.ctx = ctx;
        this.mView = mView;
        mApiClient = new ApiClient();
    }

    @Override
    public void getCategoryDetails(String catId, String subId, String depttId, String sort) {
        mView.showProgress();
        Observable<CategoryDetailResponse> categoriesCall = mApiClient.getInstance().getCategoryDetails(subId, catId, depttId, sort);
        categoriesCall.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<CategoryDetailResponse>() {
                    @Override
                    public void onCompleted() {
                        mView.hideProgress();
                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.hideProgress();
//                        mView.showError(e.getMessage());
                    }

                    @Override
                    public void onNext(CategoryDetailResponse categoryDetailResponse) {
                        mView.hideProgress();
                        if(categoryDetailResponse.getStatus().equalsIgnoreCase("true")){
                            sampleList= new ArrayList<>();
                            sampleList.addAll(categoryDetailResponse.getData().getProducts());
                            mView.refreshData(categoryDetailResponse,filterSupplier(sampleList));
                        }else {
//                            mView.showError(categoryDetailResponse.getMessage());
                            mView.onNoData(categoryDetailResponse.getMessage());
                        }
                    }
                });
    }

    @Override
    public void filterItems(String groupId, String catId, String subId, String supplierName, String min, String max, int sort) {
        mView.showProgress();
        Observable<CategoryDetailResponse> categoriesCall = mApiClient.getInstance().filterItems(groupId,catId,subId,supplierName,min,max,""+sort);
        categoriesCall.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<CategoryDetailResponse>() {
                    @Override
                    public void onCompleted() {
                        mView.hideProgress();
                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.hideProgress();
//                        mView.showError(e.getMessage());
                    }

                    @Override
                    public void onNext(CategoryDetailResponse categoryDetailResponse) {
                        mView.hideProgress();
                        if(categoryDetailResponse.getStatus().equalsIgnoreCase("true")){
                            mView.refreshData(categoryDetailResponse,filterSupplier(sampleList));
                        }else {
//                            mView.showError(categoryDetailResponse.getMessage());
                            mView.onNoData(categoryDetailResponse.getMessage());
                        }
                    }
                });


    }

    @Override
    public void addToCart(String ProductId, String Qty) {
        mView.showProgress();
        Observable addTocart = mApiClient.getInstance().addToCart(MyPreferences.getInstance().getString(MyPreferences.Key.USERID), ProductId, Qty);       //TODO
        addTocart.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<AddToCartResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.hideProgress();
                    }

                    @Override
                    public void onNext(AddToCartResponse o) {
                        mView.hideProgress();
                        mView.showError("Added to cart Successfully");
                    }
                });
    }

    private ArrayList<String> filterSupplier(final List<ProductsItem> sampleList) {
        supplierList= new ArrayList<>();
        Observable.from(sampleList).subscribe(new Subscriber<ProductsItem>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(ProductsItem productsItem) {
                supplierList.add(productsItem.getSupplierName());
                Log.i(TAG, "onNext:list "+productsItem.getSupplierName());
            }
        });
        return supplierList;
    }
}
