package com.ahujaBros.presenter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import com.ahujaBros.MyApplication;
import com.ahujaBros.adapter.OrderHistoryAdapter;
import com.ahujaBros.api.ApiClient;
import com.ahujaBros.model.cart.MyCart;
import com.ahujaBros.model.order.OrderHistoryHeader;
import com.ahujaBros.model.order.ProductInfo;
import com.ahujaBros.ui.OrderHistoryView;
import com.ahujaBros.utils.MyPreferences;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static android.content.ContentValues.TAG;
import static java.security.AccessController.getContext;

/**
 * Created by insonix on 13/6/17.
 */

public class OrderHistoryPresenterImpl implements OrderHistoryPresenter {

    OrderHistoryView mView;
    private ApiClient mApiClient;
    private ArrayList<ProductInfo> productList;
    private OrderHistoryHeader orderHistoryHeader;
    private ArrayList<OrderHistoryHeader> listDataHeader;
    private HashMap<String, List<ProductInfo>> listDataChild;


    public OrderHistoryPresenterImpl(OrderHistoryView mView) {
        this.mView = mView;
        mApiClient = new ApiClient();
    }

    @Override
    public void getOrders() {
        listDataHeader = new ArrayList<>();
        listDataChild = new HashMap<>();
        productList = new ArrayList<>();
        mView.showProgress();
        String url = ApiClient.BASE_URL + "order_history.php?user_id=" + MyPreferences.getInstance().getString(MyPreferences.Key.USERID);
        Log.d(TAG, "getOrderHistory: " + url);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject jsonObject) {
                mView.hideProgress();
                String response = null;
                try {
                    response = jsonObject.getString("status");
                    if (response.equals("true")) {
                        JSONObject response1 = jsonObject.getJSONObject("data");
                        JSONObject jsonObject1 = response1.getJSONObject("orders");
                        Iterator iterator = jsonObject1.keys();
                        while (iterator.hasNext()) {
                            productList = new ArrayList<>();
                            orderHistoryHeader = new OrderHistoryHeader();
                            String key = (String) iterator.next();
                            orderHistoryHeader.setOrderId(key);

                            JSONObject jsonArray = jsonObject1.getJSONObject(key);
                            Iterator iterator1 = jsonArray.keys();
                            while (iterator1.hasNext()) {
                                String key1 = (String) iterator1.next();
                                if (key1.equalsIgnoreCase("date")) {
                                    String strCurrentDate = jsonArray.getString(key1);
                                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                                    Date newDate = format.parse(strCurrentDate);
                                    format = new SimpleDateFormat("dd/MM/yyyy");
                                    String date = format.format(newDate);
                                    orderHistoryHeader.setDate(date);
                                } else if (key1.equalsIgnoreCase("total_amount")) {
                                    String totalammount = jsonArray.getString(key1);
                                    orderHistoryHeader.setTotalAmount(totalammount);
                                } else {
                                    JSONObject jsonObject2 = jsonArray.getJSONObject(key1);
                                    JSONObject jsonObject3 = jsonObject2.getJSONObject("product_info");
                                    if (jsonObject3.length() != 0) {
                                        ProductInfo productInfo = new ProductInfo();
                                        productInfo.setMRP(jsonObject3.getString("MRP"));
                                        productInfo.setBrandName(jsonObject3.getString("brand_name"));
                                        productInfo.setBasicRate(jsonObject3.getString("basic_rate"));
                                        productInfo.setSupplierName(jsonObject3.getString("supplier_name"));
                                        productInfo.setProductImage(jsonObject3.getString("product_image").split(",")[0]);
                                        productInfo.setStyleDescription(jsonObject3.getString("style_description"));
                                        productInfo.setColor(jsonObject3.getString("color"));
                                        productInfo.setSize(jsonObject3.getString("size"));
                                        productInfo.setQty(jsonObject2.getString("quantity"));
                                        productInfo.setStyleNo(jsonObject3.getString("style_no"));
                                        productList.add(productInfo);
                                    }
                                }
                            }
                            listDataHeader.add(orderHistoryHeader);
                            listDataChild.put(key, productList);
                            mView.onLoadingOrders(listDataHeader,listDataChild);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                mView.hideProgress();
                mView.showError(volleyError.getMessage());
            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS * 2, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        MyApplication.getInstance().addToRequestQueue(jsonObjectRequest);
    }
}

 /*   @Override
    public void getOrders() {
        mView.showProgress();
        Observable<JsonObject> myCart = mApiClient.getInstance().getOrderHistory(MyPreferences.getInstance().getString(MyPreferences.Key.USERID));
        myCart.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<JsonObject>() {
                    @Override
                    public void onCompleted() {
                        mView.hideProgress();

                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.hideProgress();
                        Log.i(TAG, "onNext:ORder History error " + e.getMessage());
                    }

                    @Override
                    public void onNext(JsonObject element) {
                        mView.hideProgress();
                        Log.i(TAG, "onNext:ORder History " + element);
                        try {
                            classifyResponse(element);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }

    private void classifyResponse(JsonObject element) throws JSONException {
        // use keys() iterator, you don't need to know what keys are there/missing
        listDataHeader = new ArrayList<>();
        listDataChild = new HashMap<>();
        productList = new ArrayList<>();
        JsonObject job2 = null;
        JsonObject job1 = element.getAsJsonObject("data");
        job2 = job1.getAsJsonObject("orders");
       *//* JsonArray jArrayObject = new JsonArray();
        try {
            jArrayObject.add(getJsonObject(job2));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.i(TAG, "classifyResponse: " + jArrayObject.size());*//*
        JSONObject jarr= null;
        try {
            jarr = new JSONObject(String.valueOf(job2));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Iterator iterator = jarr.keys();
        while (iterator.hasNext()) {
            productList = new ArrayList<>();
            orderHistoryHeader = new OrderHistoryHeader();
            String key = (String) iterator.next();
            orderHistoryHeader.setOrderId(key);

            JsonObject jsonArray = null;
            jsonArray = job2.getAsJsonObject(key);
            JsonArray jArrayObject1 = new JsonArray();
            try {
                jArrayObject1.add(getJsonObject(jsonArray));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Iterator iterator1 = jArrayObject1.iterator();
            while (iterator1.hasNext()) {
                String key1 = (String) iterator1.next();
                if (key1.equalsIgnoreCase("date")) {
                    String strCurrentDate = null;
                    strCurrentDate = jArrayObject1.getAsString();
                    String date = null;
                    try {
                        date = setDate(strCurrentDate);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    orderHistoryHeader.setDate(date);
                } else if (key1.equalsIgnoreCase("total_amount")) {
                    String totalammount = null;
                    totalammount = jsonArray.getAsString();
                    orderHistoryHeader.setTotalAmount(totalammount);
                } else {
                    JsonObject jsonObject2 = null;
                    jsonObject2 = (JsonObject) jsonArray.get(key1);
                    JsonObject jsonObject3 = (JsonObject) jsonObject2.get("product_info");
                    if (jsonObject3.size() != 0) {
                        ProductInfo productInfo = new ProductInfo();
                        productInfo.setMRP(String.valueOf(jsonObject3.get("MRP")));
                           *//* productInfo.setBrandName(jsonObject3.getString("brand_name"));
                            productInfo.setBasicRate(jsonObject3.getString("basic_rate"));
                            productInfo.setSupplierName(jsonObject3.getString("supplier_name"));
                            productInfo.setProductImage(jsonObject3.getString("product_image").split(",")[0]);
                            productInfo.setStyleDescription(jsonObject3.getString("style_description"));
                            productInfo.setColor(jsonObject3.getString("color"));
                            productInfo.setSize(jsonObject3.getString("size"));
                            productInfo.setQty(jsonObject2.getString("quantity"));
                            productInfo.setStyleNo(jsonObject3.getString("style_no"));
                            productList.add(productInfo);*//*
                    }

                }
            }
            listDataHeader.add(orderHistoryHeader);
            listDataChild.put(key, productList);
            mView.onLoadingOrders(listDataHeader, listDataChild);
        }
    }

    private String setDate(String strCurrentDate) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date newDate = format.parse(strCurrentDate);
        format = new SimpleDateFormat("dd/MM/yyyy");
        return format.format(newDate);
    }

    private static JsonObject getJsonObject(JsonObject job2) throws JSONException {
//        JSONObject jObject = new JSONObject();
        //put value jObject here..
        return job2;
    }*/