package com.ahujaBros.ui;

/**
 * Created by insonix on 16/6/17.
 */

public interface CartUpdateListener {

    void onCartChange();
}
