package com.ahujaBros.ui;

import com.ahujaBros.model.NotificationResponse.NotificationResponse;
import com.ahujaBros.ui.base.BaseView;

/**
 * Created by insonix on 8/6/17.
 */

public interface NotificationView extends BaseView{

    void onLoadingNotifications(NotificationResponse response);

}
