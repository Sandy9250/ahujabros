package com.ahujaBros.ui;

import com.ahujaBros.model.Login.LoginResponse;
import com.ahujaBros.ui.base.BaseView;

/**
 * Created by insonix on 31/5/17.
 */

public interface LoginView extends BaseView{

    boolean validateUser(String user,String pwd);

    void onSuccessLogin(LoginResponse s);
}
