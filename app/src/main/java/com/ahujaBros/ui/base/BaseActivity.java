package com.ahujaBros.ui.base;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;

import com.ahujaBros.BusEvents.GlobalBus;
import com.ahujaBros.utils.CommonUtils;
import com.ahujaBros.utils.NetworkUtil;
import com.src.ahujaBros.R;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;


/**
 * Created by insonix on 23/5/17.
 */

public class BaseActivity extends AppCompatActivity implements BaseView {

    private Dialog dialog;

    @Subscribe(threadMode = ThreadMode.MAIN)
    @Override
    protected void onStart() {
        super.onStart();
        GlobalBus.getBus().register(this);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (NetworkUtil.getConnectivityStatus(this) == 0) {
            if (!CommonUtils.showNetworkUnavailable(this)) {
                return;
            }
        }
        dialog = new Dialog(this, android.R.style.Theme_Translucent);
        View views = LayoutInflater.from(this).inflate(R.layout.dot_dialog, null);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(views);
    }


    @Override
    protected void onStop() {
        GlobalBus.getBus().unregister(this);
        super.onStop();
    }

    @Override
    public void showProgress() {
        if (NetworkUtil.getConnectivityStatus(this) != 0) {
            dialog.show();
        } else {
            CommonUtils.showNetworkUnavailable(this);
        }
    }

    @Override
    public void hideProgress() {
        dialog.dismiss();
    }

    @Override
    public void showError(String s) {
        CommonUtils.showActivitySnack(getWindow(), s);
    }


}
