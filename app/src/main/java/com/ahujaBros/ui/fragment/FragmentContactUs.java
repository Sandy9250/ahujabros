package com.ahujaBros.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ahujaBros.ui.activity.MainActivity;
import com.ahujaBros.ui.base.BaseFragment;
import com.ahujaBros.utils.AddToolbarTitle;
import com.ahujaBros.utils.CommonUtils;
import com.src.ahujaBros.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by insonix on 12/6/17.
 */

public class FragmentContactUs extends BaseFragment {


    @BindView(R.id.map)
    ImageView map;
    Unbinder unbinder;
    @BindView(R.id.get)
    TextView getTv;
    @BindView(R.id.address)
    TextView address;
    @BindView(R.id.txtaddress)
    TextView txtaddress;
    private AddToolbarTitle mTitleListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mTitleListener= ((MainActivity)getActivity());
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_contactus, container, false);
        unbinder = ButterKnife.bind(this, v);
        mTitleListener.changeToolbarTitle(getString(R.string.contact_us));
        setHasOptionsMenu(true);
        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.findItem(R.id.search).setVisible(false);
    }

    @OnClick(R.id.map)
    public void onViewClicked() {
        CommonUtils.addFragment(getActivity(),new MapFragment(),false,"Map",null);
    }
}
