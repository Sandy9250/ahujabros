package com.ahujaBros.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ahujaBros.BusEvents.Events;
import com.ahujaBros.BusEvents.GlobalBus;
import com.ahujaBros.model.cart.DataItem;
import com.ahujaBros.model.cart.MyCart;
import com.ahujaBros.model.cart.OrderResponse;
import com.ahujaBros.presenter.BagPresenter;
import com.ahujaBros.presenter.BagPresenterImpl;
import com.ahujaBros.ui.CartView;
import com.ahujaBros.ui.activity.MainActivity;
import com.ahujaBros.ui.base.BaseFragment;
import com.ahujaBros.utils.AddToolbarTitle;
import com.ahujaBros.utils.CommonUtils;
import com.squareup.picasso.Picasso;
import com.src.ahujaBros.R;

import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import rx.Observable;
import rx.Subscriber;

import static android.content.ContentValues.TAG;

/**
 * Created by Insonix on 8/8/2016.
 */
public class PaymentCartScreen extends BaseFragment implements CartView {

    @BindView(R.id.image_product)
    ImageView imageProduct;
    @BindView(R.id.linear)
    LinearLayout linear;
    @BindView(R.id.order_details)
    TextView orderDetails;
    @BindView(R.id.product_name)
    TextView productName;
    @BindView(R.id.supplier_name)
    TextView supplierName;
    @BindView(R.id.you_pay)
    TextView youPay;
    @BindView(R.id.product_price)
    Button productPrice;
    @BindView(R.id.proceed)
    Button proceed;
    Unbinder unbinder;
    BagPresenter mPresenter;
    private Events.SelectedItemDetailEvent selectedItemEvent;
    private AddToolbarTitle mTitleListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mTitleListener = ((MainActivity) getActivity());
    }

    @Subscribe
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        setRetainInstance(true);
        super.onCreate(savedInstanceState);

    }

    @Subscribe
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.payment_cart_screen, container, false);
        unbinder = ButterKnife.bind(this, view);
        mTitleListener.changeToolbarTitle(getString(R.string.my_cart));
        selectedItemEvent = GlobalBus.getBus().getStickyEvent(Events.SelectedItemDetailEvent.class);
        initData();
        mPresenter = new BagPresenterImpl(this);
        return view;
    }

    private void initData() {
        Picasso.with(getActivity()).load(selectedItemEvent.getmItem().getProductImage().split(",")[0]).error(R.drawable.not_avlbl).resize(400, 400).into(imageProduct);
        productName.setText(selectedItemEvent.getmItem().getBrandName());
        productPrice.setText(getResources().getString(R.string.Rs) + " " + selectedItemEvent.getmItem().getMRP());
        supplierName.setText("By: " + selectedItemEvent.getmItem().getSupplierName());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.proceed)
    public void onViewClicked() {
        mPresenter.orderCart(selectedItemEvent.getmItem().getId() + "-0-" + selectedItemEvent.getQuantity());
    }

    @Override
    public void onFetchCart(MyCart cart) {

    }

    @Override
    public void onSuccessRemove(MyCart cart) {

    }

    @Override
    public void noDataAvailable() {

    }

    @Override
    public void onOrderSuccess(OrderResponse response) {
        CommonUtils.showActivitySnack(getActivity().getWindow(), getResources().getString(R.string.order_success));
        CommonUtils.addFragment(getActivity(), new ThanksFragment(), false, "Payment", null);
    }


    /**
     * Method to append cart products in a string "order"
     *
     * @param mCartDetail
     * @return String order
     */
    //order is a string separated with comma(productId-bag_id-qty)
    private String getOrderString(final MyCart mCartDetail) {
        final StringBuffer orderString = new StringBuffer();
        Observable.from(mCartDetail.getData()).subscribe(new Subscriber<DataItem>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(DataItem productsItem) {
                Log.i(TAG, "onNext:list " + productsItem.getBagId());
                orderString.append(productsItem.getProductId() + "-" + productsItem.getBagId() + "-" + productsItem.getQty() + ",");
            }
        });
        return orderString.toString();
    }
}
