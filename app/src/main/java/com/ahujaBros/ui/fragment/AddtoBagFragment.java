package com.ahujaBros.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.LayoutAnimationController;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.ahujaBros.BusEvents.Events;
import com.ahujaBros.BusEvents.GlobalBus;
import com.ahujaBros.adapter.CartAdapter;
import com.ahujaBros.model.cart.DataItem;
import com.ahujaBros.model.cart.MyCart;
import com.ahujaBros.model.cart.OrderResponse;
import com.ahujaBros.presenter.BagPresenter;
import com.ahujaBros.presenter.BagPresenterImpl;
import com.ahujaBros.ui.CartUpdateListener;
import com.ahujaBros.ui.CartView;
import com.ahujaBros.ui.activity.MainActivity;
import com.ahujaBros.ui.base.BaseFragment;
import com.ahujaBros.utils.AddToolbarTitle;
import com.ahujaBros.utils.CommonUtils;
import com.ahujaBros.utils.MyPreferences;
import com.ahujaBros.utils.RecyclerItemClickListener;
import com.src.ahujaBros.R;

import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import rx.Observable;
import rx.Subscriber;

import static android.content.ContentValues.TAG;

/**
 * Created by insonix on 5/6/17.
 */

public class AddtoBagFragment extends BaseFragment implements CartView, RecyclerItemClickListener {

    @BindView(R.id.text_internet)
    TextView textInternet;
    @BindView(R.id.text_no_data)
    TextView textNoData;
    @BindView(R.id.no_data_layout)
    LinearLayout noDataLayout;
    @BindView(R.id.total_items)
    TextView totalItems;
    @BindView(R.id.total_payable)
    TextView totalPayable;
    @BindView(R.id.header)
    LinearLayout header;
    @BindView(R.id.cartitems_list)
    RecyclerView cartitemsList;
    @BindView(R.id.price_details)
    TextView priceDetails;
    @BindView(R.id.delivery)
    TextView delivery;
    @BindView(R.id.total_amount)
    TextView totalAmount;
    @BindView(R.id.payment_layout)
    LinearLayout paymentLayout;
    @BindView(R.id.scrollview)
    ScrollView scrollview;
    @BindView(R.id.buy_btn)
    Button buyBtn;
    @BindView(R.id.relative_lay)
    RelativeLayout relativeLay;
    Unbinder unbinder;
    @BindView(R.id.keepshopping_btn)
    Button keepshoppingBtn;
    @BindView(R.id.bottom_buttons)
    LinearLayout bottomButtons;
    @BindView(R.id.shopping_bttn)
    Button shoppingBttn;
    private CartAdapter adapter;
    List<DataItem> mCart;
    private LayoutAnimationController controller;
    private BagPresenter mPresenter;
    MyCart mCartDetail;
    AddToolbarTitle mTitleListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mTitleListener = ((MainActivity) getActivity());
    }

    @Subscribe
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_add_to_bag, container, false);
        unbinder = ButterKnife.bind(this, view);
        mPresenter = new BagPresenterImpl(this);
        initRecyclerView();
        mTitleListener.changeToolbarTitle("My Cart");
        return view;
    }

    public static void setListener(CartUpdateListener listener) {
        MainActivity.cartListener = listener;
    }

    private void initRecyclerView() {
        mCart = new ArrayList<>();
        adapter = new CartAdapter(mCart, getActivity(), this);
        cartitemsList.setLayoutManager(new LinearLayoutManager(getActivity()));
        cartitemsList.setLayoutAnimation(initAnim());
        cartitemsList.setAdapter(adapter);
    }

    private LayoutAnimationController initAnim() {
        AnimationSet set = new AnimationSet(true);

        Animation animation = new AlphaAnimation(0.0f, 1.0f);
        animation.setDuration(1500);
        set.addAnimation(animation);

        animation = new TranslateAnimation(
                Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF, 0.0f,
                Animation.RELATIVE_TO_SELF, -1.0f, Animation.RELATIVE_TO_SELF, 0.0f
        );
        animation.setDuration(100);
        set.addAnimation(animation);
        controller = new LayoutAnimationController(set, 0.5f);
        return controller;
    }


    @Override
    public void onResume() {
        super.onResume();
        try {
            mPresenter.fetchCart();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onFetchCart(MyCart cart) {
        //To get cart count on Home
        GlobalBus.getBus().postSticky(new Events.CartCountEvent(cart.getData().size()));
        MyPreferences.getInstance(getActivity()).put(MyPreferences.Key.CART_COUNT, cart.getData().size());
        ((MainActivity) getActivity()).updateCartCount();            //Update cart badge
        header.setVisibility(View.VISIBLE);
        paymentLayout.setVisibility(View.VISIBLE);
        totalItems.setVisibility(View.VISIBLE);
        priceDetails.setVisibility(View.VISIBLE);
        bottomButtons.setVisibility(View.VISIBLE);
        Log.i(TAG, "onFetchCart: " + cart.getData().size());
        adapter.refreshData(cart.getData());
        String s = "Items: " + "<b>" + cart.getData().size() + "</b>";
        totalItems.setText(Html.fromHtml(s));
        String price = "Amount: " + getResources().getString(R.string.Rs) + " " + "<b>" + cart.getTotalPayable() + "</b>";
        totalPayable.setText(Html.fromHtml(price));
        totalAmount.setText(getResources().getString(R.string.Rs) + " " + cart.getTotalPayable());
        mCartDetail = cart;
    }

    @Override
    public void onSuccessRemove(MyCart cart) {
        if (cart.getStatus().equals("false")) {
            noDataAvailable();
            GlobalBus.getBus().postSticky(new Events.CartCountEvent(0));
        } else {
            GlobalBus.getBus().postSticky(new Events.CartCountEvent(cart.getData().size()));
            ((MainActivity) getActivity()).updateCartCount();
            adapter.refreshData(cart.getData());
            onFetchCart(cart);
        }
    }

    @Override
    public void noDataAvailable() {
        relativeLay.setVisibility(View.GONE);
        bottomButtons.setVisibility(View.GONE);
        totalItems.setVisibility(View.GONE);
        priceDetails.setVisibility(View.GONE);
        paymentLayout.setVisibility(View.GONE);
        noDataLayout.setVisibility(View.VISIBLE);
        MyPreferences.getInstance(getActivity()).put(MyPreferences.Key.CART_COUNT, 0);
        GlobalBus.getBus().postSticky(new Events.CartCountEvent(0));
        ((MainActivity) getActivity()).updateCartCount();
    }

    @Override
    public void onOrderSuccess(OrderResponse response) {
        GlobalBus.getBus().postSticky(new Events.CartCountEvent(0));
        ((MainActivity) getActivity()).updateCartCount();
        CommonUtils.showActivitySnack(getActivity().getWindow(), getString(R.string.order_success));
        CommonUtils.addFragment(getActivity(), new ThanksFragment(), false, "Thanks", null);
    }

    @Override
    public void onRemoveItemClick(DataItem item) {
        mPresenter.removeCartItem(item.getBagId());
    }

    @Override
    public void onEditIemClick(DataItem item) {
        GlobalBus.getBus().postSticky(new Events.CartItemEvent(item));
        CommonUtils.addFragment(getActivity(), new InItemDetailFragment(), false, "InItemDetail", null);
    }

    @OnClick(R.id.shopping_bttn)
    public void onViewClicked() {
        getActivity().startActivity(MainActivity.createInstance(getContext()));
        getActivity().finish();
    }

    @OnClick(R.id.buy_btn)
    public void onBuyViewClicked() {
        mPresenter.orderCart(getOrderString(mCartDetail));
    }

    /**
     * Method to append cart products in a string "order"
     *
     * @param mCartDetail
     * @return String order
     */
    //order is a string separated with comma(productId-bag_id-qty)
    private String getOrderString(final MyCart mCartDetail) {
        final StringBuffer orderString = new StringBuffer();
        Observable.from(mCartDetail.getData()).subscribe(new Subscriber<DataItem>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(DataItem productsItem) {
                Log.i(TAG, "onNext:list " + productsItem.getBagId());
                orderString.append(productsItem.getProductId() + "-" + productsItem.getBagId() + "-" + productsItem.getQty() + ",");
            }
        });
        return orderString.toString();
    }

    @OnClick(R.id.keepshopping_btn)
    public void onKeepViewClicked() {
        CommonUtils.addFragment(getActivity(),new HomeFragment(),false,"Home",null);
    }
}
