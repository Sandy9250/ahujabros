package com.ahujaBros.ui.activity;

import android.annotation.TargetApi;
import android.app.ActivityOptions;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.transition.TransitionManager;
import android.support.v7.app.AppCompatActivity;
import android.transition.Explode;
import android.transition.Slide;
import android.transition.Transition;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.ahujaBros.utils.CommonUtils;
import com.ahujaBros.utils.MyPreferences;
import com.src.ahujaBros.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SplashActivity extends AppCompatActivity {

    private ViewGroup viewGroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            Transition reenterTrans = new Slide();
            reenterTrans.setDuration(3000);
            ((Slide) reenterTrans).setSlideEdge(Gravity.LEFT);
            getWindow().setExitTransition(reenterTrans);
            reenterTrans.addListener(new Transition.TransitionListener() {
                @Override
                public void onTransitionStart(Transition transition) {

                }

                @Override
                public void onTransitionEnd(Transition transition) {
                    finish();
                }

                @Override
                public void onTransitionCancel(Transition transition) {

                }

                @Override
                public void onTransitionPause(Transition transition) {

                }

                @Override
                public void onTransitionResume(Transition transition) {

                }
            });
        }
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        viewGroup = (ViewGroup) findViewById(R.id.main_layout);
        viewGroup.setBackgroundColor(getResources().getColor(R.color.white));
        viewGroup.setOnClickListener(new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View view) {
                ActivityOptions options = null;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    options = ActivityOptions.makeSceneTransitionAnimation(
                            SplashActivity.this);
                }
                Intent intent = new Intent(SplashActivity.this, SplashAnimated.class);
                startActivity(intent, options.toBundle());
                finish();
            }
        });
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                viewGroup.performClick();
            }
        }, 2000);
    }

}
