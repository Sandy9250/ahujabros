package com.ahujaBros.adapter;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.ahujaBros.utils.SortItemClickListener;
import com.src.ahujaBros.R;

/**
 * Created by insonix on 30/5/17.
 */

public class SortListAdapter extends BaseAdapter {
    Context ctx;
    String[] items;
    SortItemClickListener mListener;

    public SortListAdapter(Context ctx, String[] items,SortItemClickListener Listener) {
        this.ctx = ctx;
        this.items = items;
        mListener=Listener;
    }


    @Override
    public int getCount() {
        return items.length;
    }

    @Override
    public Object getItem(int position) {
        return items[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.sort_item_list, parent, false);
        TextView textView = (TextView) rowView.findViewById(R.id.sort_text);
        LinearLayout rowLayout = (LinearLayout) rowView.findViewById(R.id.parent);
        textView.setText(items[position]);
        rowLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onListItemClick(position);
            }
        });
        return rowView;
    }
}
