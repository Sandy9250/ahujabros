package com.ahujaBros.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.src.ahujaBros.R;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by insonix on 24/5/17.
 */

public class CustomPagerAdapter extends PagerAdapter {

    List<String> mPaths;
    Context mCtx;
    LayoutInflater inflater;

    public CustomPagerAdapter(Context context, List<String> path) {
        mCtx=context;
        mPaths=path;
    }

    @Override
    public int getCount() {
        return mPaths.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view==object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        inflater = (LayoutInflater) mCtx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = inflater.inflate(R.layout.viewpager_item, container,
                false);
        ImageView imgflag = (ImageView) itemView.findViewById(R.id.imgView);
        Picasso.with(mCtx).load(mPaths.get(position)).into(imgflag);
        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(View collection, int position, Object view) {
        ((ViewPager) collection).removeView((View) view);
    }


}
