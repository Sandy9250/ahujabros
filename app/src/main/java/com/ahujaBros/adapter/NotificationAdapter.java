package com.ahujaBros.adapter;

import android.content.Context;
import android.os.Build;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ahujaBros.model.NotificationResponse.DataItem;
import com.ahujaBros.utils.CircleTransform;
import com.ahujaBros.utils.CommonUtils;
import com.ahujaBros.utils.RecyclerItemGenricClickListener;
import com.squareup.picasso.Picasso;
import com.src.ahujaBros.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import worldline.com.foldablelayout.FoldableLayout;

import static android.content.ContentValues.TAG;

/**
 * Created by insonix on 8/6/17.
 */

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.NotificationViewHolder> {

    ArrayList<DataItem> mItem;
    Context mCtx;
    RecyclerItemGenricClickListener mListener;
    private Map<Integer, Boolean> mFoldStates = new HashMap<>();

    public NotificationAdapter(ArrayList<DataItem> responseItems, RecyclerItemGenricClickListener listener) {
        mItem = responseItems;
        mListener = listener;
    }

    @Override
    public NotificationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mCtx = parent.getContext();
        return new NotificationViewHolder(new FoldableLayout(parent.getContext()));
    }

    @Override
    public void onBindViewHolder(final NotificationViewHolder holder, final int position) {
        holder.msg.setText(mItem.get(position).getMsg());
        holder.itemTime.setText(CommonUtils.formatDate(mItem.get(position).getSentOn().split(" ")[0]) + " at " + mItem.get(position).getSentOn().split(" ")[1]);
        holder.txtMsg.setText(mItem.get(position).getMsg());
        holder.sent.setText(CommonUtils.formatDate(mItem.get(position).getSentOn().split(" ")[0]) + " at " + mItem.get(position).getSentOn().split(" ")[1]);
        holder.txtHeader.setText(mItem.get(position).getSubject());
        holder.itemName.setText(mItem.get(position).getSubject());
        if (mItem.get(position).getStatus().equalsIgnoreCase("1")) {
            holder.mainCard.setBackgroundColor(mCtx.getResources().getColor(R.color.white));
        } else {
            holder.mainCard.setBackgroundColor(mCtx.getResources().getColor(R.color.light_grey));
        }
        if (!TextUtils.isEmpty(mItem.get(position).getFilename())) {
            Picasso.with(holder.mFoldableLayout.getContext()).load(mItem.get(position).getFilename()).error(R.drawable.not_avlbl).resize(100, 100).transform(new CircleTransform()).into(holder.thumbnail);
            Picasso.with(holder.mFoldableLayout.getContext()).load(mItem.get(position).getFilename()).error(R.drawable.not_avlbl).resize(180, 200).into(holder.imageviewDetail);
        } else {
            holder.thumbnail.setImageResource(R.drawable.app_logo);
            holder.imageviewDetail.setImageResource(R.drawable.app_logo);
        }

        // Bind state
        if (!holder.mFoldableLayout.isFolded()) {
            Log.i(TAG, "onBindViewHolder:Keep it open ");
        }

        if (mFoldStates.containsKey(position)) {
            if (mFoldStates.get(position) == Boolean.TRUE) {
                if (!holder.mFoldableLayout.isFolded()) {
                    holder.mFoldableLayout.foldWithoutAnimation();
                }
            } else if (mFoldStates.get(position) == Boolean.FALSE) {
                if (holder.mFoldableLayout.isFolded()) {
                    holder.mFoldableLayout.unfoldWithoutAnimation();
                }
            }
        } else {
            holder.mFoldableLayout.foldWithoutAnimation();
        }

        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.mFoldableLayout.isFolded()) {
                    if(mItem.get(position).getStatus().equalsIgnoreCase("0")){
                        mListener.onItemClick(position);
                    }
                    holder.mFoldableLayout.unfoldWithAnimation();
                } else {
                    holder.mFoldableLayout.foldWithAnimation();
                }
            }
        });
        holder.Detailcard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.mFoldableLayout.isFolded()) {
                    holder.mFoldableLayout.unfoldWithAnimation();
                } else {
                    holder.mFoldableLayout.foldWithAnimation();
                }
            }
        });
        holder.mFoldableLayout.setFoldListener(new FoldableLayout.FoldListener() {
            @Override
            public void onUnFoldStart() {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    holder.mFoldableLayout.setElevation(5);
                }
            }

            @Override
            public void onUnFoldEnd() {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    holder.mFoldableLayout.setElevation(0);
                }
                mFoldStates.put(holder.getAdapterPosition(), false);
            }

            @Override
            public void onFoldStart() {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    holder.mFoldableLayout.setElevation(5);
                }
            }

            @Override
            public void onFoldEnd() {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    holder.mFoldableLayout.setElevation(0);
                }
                mFoldStates.put(holder.getAdapterPosition(), true);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mItem.size();
    }

    public void refresh(List<DataItem> data) {
        mItem.clear();
        mItem.addAll(data);
        notifyDataSetChanged();
    }


    public static class NotificationViewHolder extends RecyclerView.ViewHolder {

        protected FoldableLayout mFoldableLayout;

        @BindView(R.id.thumbnail)
        ImageView thumbnail;
        @BindView(R.id.msg)
        TextView msg;
        @BindView(R.id.item_name)
        TextView itemName;
        @BindView(R.id.timestamp)
        TextView itemTime;
        @BindView(R.id.content_layout)
        LinearLayout contentLayout;
        @BindView(R.id.parent_layout)
        LinearLayout parentLayout;
        @BindView(R.id.imageview_detail)
        ImageView imageviewDetail;
        @BindView(R.id.txt_msg)
        TextView txtMsg;
        @BindView(R.id.sent)
        TextView sent;
        @BindView(R.id.Detailcard)
        CardView Detailcard;
        @BindView(R.id.txt_header)
        TextView txtHeader;
        @BindView(R.id.main_card)
        CardView mainCard;

        public NotificationViewHolder(FoldableLayout foldableLayout) {
            super(foldableLayout);
            mFoldableLayout = foldableLayout;
            foldableLayout.setupViews(R.layout.item_notification_list, R.layout.item_noti_detail, R.dimen.card_cover_height, itemView.getContext());
            ButterKnife.bind(this, foldableLayout);
        }
    }

}
