package com.ahujaBros.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ahujaBros.model.cart.DataItem;
import com.ahujaBros.ui.fragment.AddtoBagFragment;
import com.ahujaBros.utils.RecyclerItemClickListener;
import com.squareup.picasso.Picasso;
import com.src.ahujaBros.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by insonix on 5/6/17.
 */

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.CartViewHolder> {

    List<DataItem> mCart;
    Context mCtx;
    RecyclerItemClickListener mListener;
    LayoutInflater mInflater;

    public CartAdapter(List<DataItem> cart, Context ctx, AddtoBagFragment fragment) {
        this.mCart = cart;
        mCtx = ctx;
        mListener=fragment;
    }

    @Override
    public CartViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mInflater = LayoutInflater.from(parent.getContext());
        View v = mInflater.inflate(R.layout.bag_item_list, parent, false);
        return new CartViewHolder(v);
    }

    @Override
    public void onBindViewHolder(CartViewHolder holder, final int position) {
        String[] imgPaths = mCart.get(position).getImageUrl().split(",");
        Picasso.with(mCtx).load(imgPaths[0]).error(R.drawable.not_avlbl).resize(150, 150).into(holder.productImage);
        holder.productName.setText(mCart.get(position).getBrand());
        holder.quantity.setText("Quantity: " + mCart.get(position).getQty());
        holder.mrp.setText("Price: "+mCtx.getString(R.string.Rs) + mCart.get(position).getMRP());
        holder.setPrice.setText("Set Price: "+mCtx.getString(R.string.Rs) + mCart.get(position).getBasicRate());
        holder.sizeOfSet.setText("Size of Set: " + mCart.get(position).getSize());
        holder.removeItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onRemoveItemClick(mCart.get(position));
            }
        });
        holder.editItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onEditIemClick(mCart.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return mCart.size();
    }

    public void refreshData(List<DataItem> data) {

        mCart.clear();
        mCart.addAll(data);
        notifyDataSetChanged();
    }

    public class CartViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.product_image)
        ImageView productImage;
        @BindView(R.id.product_name)
        TextView productName;
        @BindView(R.id.quantity)
        TextView quantity;
        @BindView(R.id.mrp)
        TextView mrp;
        @BindView(R.id.remove_item)
        TextView removeItem;
        @BindView(R.id.edit_item)
        TextView editItem;
        @BindView(R.id.setPrice)
        TextView setPrice;
        @BindView(R.id.sizeOfSet)
        TextView sizeOfSet;
        @BindView(R.id.remove_btn)
        LinearLayout removeBtn;
        @BindView(R.id.edit_btn)
        LinearLayout editBtn;
        @BindView(R.id.main)
        LinearLayout main;

        public CartViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
