package com.ahujaBros.adapter;

import android.app.Dialog;
import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;

import com.ahujaBros.model.cart.DataItem;
import com.squareup.picasso.Picasso;
import com.ahujaBros.model.CategoryDetail.ProductsItem;
import com.src.ahujaBros.R;

import java.util.List;

import me.relex.circleindicator.CircleIndicator;


/**
 * Created by insonix on 29/5/17.
 */

public class ItemDetailAdapter extends PagerAdapter {
    ProductsItem mPaths;
    DataItem mDPaths;
    Context mCtx;
    LayoutInflater inflater;
    String[] paths;

    public ItemDetailAdapter(Context context, ProductsItem path, DataItem item) {
        mCtx = context;
        mPaths = path;
        mDPaths = item;
        if (mPaths == null) {
            paths = mDPaths.getImageUrl().split(",");
        } else {
            paths = mPaths.getProductImage().split(",");
        }
    }

    @Override
    public int getCount() {
        return paths.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(final ViewGroup container, final int position) {
        inflater = (LayoutInflater) mCtx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = inflater.inflate(R.layout.viewpager_item, container,
                false);
        ImageView imgflag = (ImageView) itemView.findViewById(R.id.imgView);
        imgflag.setAdjustViewBounds(true);
        imgflag.setScaleType(ImageView.ScaleType.FIT_XY);
        Picasso.with(mCtx).load(paths[position]).into(imgflag);
        imgflag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initDialog(container.getContext());
            }
        });
        container.addView(itemView);
        return itemView;
    }

    private void initDialog(Context context) {
        final Dialog dialog = new Dialog(context, R.style.CustomTheme);
        dialog.setContentView(R.layout.custom_full_img);
        ViewPager fullImg = (ViewPager) dialog.findViewById(R.id.viewPager);
        CircleIndicator indicator = (CircleIndicator) dialog.findViewById(R.id.indicator);
        ImageView close = (ImageView) dialog.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        fullImg.setAdapter(new ItemDialogDetailAdapter(context, paths));
        indicator.setViewPager(fullImg);
        dialog.show();
    }

    @Override
    public void destroyItem(View collection, int position, Object view) {
        ((ViewPager) collection).removeView((View) view);
    }

}
