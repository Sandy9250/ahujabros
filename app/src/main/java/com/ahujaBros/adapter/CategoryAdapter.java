package com.ahujaBros.adapter;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ahujaBros.model.menu.SubCatItem;
import com.ahujaBros.ui.fragment.ThanksFragment;
import com.ahujaBros.utils.AddtoCartItemGenricClickListener;
import com.ahujaBros.utils.CommonUtils;
import com.ahujaBros.utils.RecyclerItemGenricClickListener;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.ahujaBros.BusEvents.Events;
import com.ahujaBros.BusEvents.GlobalBus;
import com.ahujaBros.MyApplication;
import com.ahujaBros.model.CategoryDetail.ProductsItem;
import com.ahujaBros.ui.CategoryView;
import com.ahujaBros.ui.FragmentNavigationManager;
import com.ahujaBros.ui.activity.MainActivity;
import com.ahujaBros.ui.fragment.InItemDetailFragment;
import com.src.ahujaBros.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by insonix on 25/5/17.
 */

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder> {

    private static final String TAG = "category";
    CategoryView mView;
    List<ProductsItem> mData;
    MainActivity ctx;
    AddtoCartItemGenricClickListener mListener;
    String title;

    public CategoryAdapter(MainActivity context, List<ProductsItem> categoryData, CategoryView view, AddtoCartItemGenricClickListener listener, String tabTitle) {
        mView = view;
        ctx=context;
        mData = categoryData;
        mListener= listener;
        title=tabTitle;
    }

    @Override
    public CategoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_category_detail, parent, false);
        return new CategoryViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final CategoryViewHolder holder, final int position) {
        String[] imgPaths = mData.get(position).getProductImage().split(",");
        String path;
        path = MyApplication.getPathForSplChars(imgPaths[0]);
        final String finalPath = path;
        Picasso.with(ctx).load(path).placeholder(R.drawable.not_avlbl).resize(450, 500).networkPolicy(NetworkPolicy.OFFLINE).into(holder.categoryImg, new Callback() {
            @Override
            public void onSuccess() {
                holder.priceText.setText(ctx.getResources().getString(R.string.Rs) + " " + Float.parseFloat(mData.get(position).getMRP())+" "+mData.get(position).getSize());
                holder.itemTitle.setText(mData.get(position).getBrandName());
                holder.more.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.i(TAG, "onClick: hiddem");
                        if (holder.hidden.getVisibility() == View.VISIBLE) {
                            holder.hidden.setVisibility(View.GONE);
                        } else {
                            holder.hidden.setVisibility(View.VISIBLE);
                        }
                    }
                });
                holder.hidden.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mListener.onItemClick(mData.get(position));
                    }
                });
                holder.innerLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.d(TAG, "onClick: Category ITem Clicked"+title);
                        GlobalBus.getBus().postSticky(new Events.CategoryToItemDetailEvent(mData.get(position),title));
                        GlobalBus.getBus().postSticky(new Events.CartItemEvent(null));       //Clear cartEvent
                        CommonUtils.addFragment(ctx,InItemDetailFragment.getInstance(),false,"InItemDetailFragment",null);
                    }
                });
            }

            @Override
            public void onError() {
                Picasso.with(ctx)
                        .load(finalPath)
                        .error(R.drawable.not_avlbl)
                        .into(holder.categoryImg, new Callback() {
                            @Override
                            public void onSuccess() {
                                holder.priceText.setText(ctx.getResources().getString(R.string.Rs) + " " + Float.parseFloat(mData.get(position).getMRP()));
                                holder.itemTitle.setText(mData.get(position).getBrandName());
                                holder.more.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Log.i(TAG, "onClick: hidden");
                                        if (holder.hidden.getVisibility() == View.VISIBLE) {
                                            holder.hidden.setVisibility(View.GONE);
                                        } else {
                                            holder.hidden.setVisibility(View.VISIBLE);
                                        }
                                    }
                                });
                                holder.parentFrame.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Log.d(TAG, "onClick: Category ITem Clicked");
                                        GlobalBus.getBus().postSticky(new Events.CategoryToItemDetailEvent(mData.get(position),title));
                                        GlobalBus.getBus().postSticky(new Events.CartItemEvent(null));       //Clear cartEvent
                                        CommonUtils.addFragment(ctx,InItemDetailFragment.getInstance(),false,"InItemDetailFragment",null);
                                    }
                                });
                            }

                            @Override
                            public void onError() {
                                Log.v("Picasso", "Could not fetch image");
                            }
                        });
            }
        });
    }



    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class CategoryViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.hidden)
        RelativeLayout hidden;
        @BindView(R.id.category_img)
        ImageView categoryImg;
        @BindView(R.id.item_title)
        TextView itemTitle;
        @BindView(R.id.price_text)
        TextView priceText;
        @BindView(R.id.more)
        ImageView more;
        @BindView(R.id.parentFrame)
        FrameLayout parentFrame;
        @BindView(R.id.inner_layout)
        RelativeLayout innerLayout;

        public CategoryViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
