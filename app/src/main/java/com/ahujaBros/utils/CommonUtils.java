package com.ahujaBros.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.Window;
import android.widget.Toast;

import com.ahujaBros.MyApplication;
import com.ahujaBros.ui.activity.SplashActivity;
import com.src.ahujaBros.BuildConfig;
import com.src.ahujaBros.R;

import java.util.List;

/**
 * Created by insonix on 29/5/17.
 */

public class CommonUtils {

    private static boolean now;

    public static void showActivitySnack(Window window, String s) {
        Snackbar.make(window.getDecorView(), s, Snackbar.LENGTH_LONG).show();
    }

    public static void showErrorSnack(Window window, int s) {
        Snackbar.make(window.getDecorView(), s, Snackbar.LENGTH_LONG).show();
    }

    public static void addFragment(FragmentActivity activity, Fragment fragment, boolean allowStateLoss, String title, Bundle bundle) {
        FragmentManager fm = activity.getSupportFragmentManager();

        if (bundle != null) {
            fragment.setArguments(bundle);
        }
        @SuppressLint("CommitTransaction")
        FragmentTransaction ft = fm.beginTransaction()
                .add(R.id.container, fragment);
        ft.addToBackStack(title);

        if (allowStateLoss || !BuildConfig.DEBUG) {
            ft.commitAllowingStateLoss();
        } else {
            ft.commit();
        }

        fm.executePendingTransactions();
    }

    public static final String formatDate(String s) {
        switch (s.split("-")[1]) {
            case "01":
                return "January " + s.split("-")[0];
            case "02":
                return "February " + s.split("-")[0];
            case "03":
                return "March " + s.split("-")[0];
            case "04":
                return "April " + s.split("-")[0];
            case "05":
                return "May " + s.split("-")[0];
            case "06":
                return "June " + s.split("-")[0];
            case "07":
                return "July " + s.split("-")[0];
            case "08":
                return "August " + s.split("-")[0];
            case "09":
                return "September " + s.split("-")[0];
            case "10":
                return "October " + s.split("-")[0];
            case "11":
                return "November " + s.split("-")[0];
            case "12":
                return "December " + s.split("-")[0];
            default:
                return "";
        }
    }

    public static void setBadgeCount(Context context, LayerDrawable icon, String count, int item) {

        BadgeDrawable badge;
        Drawable reuse;

        // Reuse drawable if possible
        if (item == 0) {
            reuse = icon.findDrawableByLayerId(R.id.ic_cart_badge);
        } else {
            reuse = icon.findDrawableByLayerId(R.id.ic_noti_badge);
        }
        if (reuse != null && reuse instanceof BadgeDrawable) {
            badge = (BadgeDrawable) reuse;
        } else {
            badge = new BadgeDrawable(context);
        }

        badge.setCount(count);
        icon.mutate();
        if (item == 0) {
            icon.setDrawableByLayerId(R.id.ic_cart_badge, badge);
        } else {
            icon.setDrawableByLayerId(R.id.ic_noti_badge, badge);
        }
    }


    public static final void shareItems(FragmentActivity ctx) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT,
                "Hey check out this item at: https://play.google.com/store/apps/details?id=com.ahujaBros&hl=en");
        sendIntent.setType("text/plain");
        ctx.startActivity(sendIntent);
    }

    //Logout
    public static void logoutApp(final Activity ctx) {
        AlertDialog.Builder dial = new AlertDialog.Builder(ctx);
        dial.setMessage("Are you sure you want to logout?");
        dial.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                MyPreferences.getInstance(MyApplication.getInstance()).clearPrefs();
                Intent intent = new Intent(ctx, SplashActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                ctx.startActivity(intent);
                ctx.finish();
            }
        });
        dial.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dial.show();
    }

    //to set badge count on app launcher icon

    public static void setBadge(Context context, int count) {
        String launcherClassName = getLauncherClassName(context);
        if (launcherClassName == null) {
            return;
        }
        Intent intent = new Intent("android.intent.action.BADGE_COUNT_UPDATE");
        intent.putExtra("badge_count", count);
        intent.putExtra("badge_count_package_name", context.getPackageName());
        intent.putExtra("badge_count_class_name", launcherClassName);
        context.sendBroadcast(intent);
    }

    public static String getLauncherClassName(Context context) {

        PackageManager pm = context.getPackageManager();

        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);

        List<ResolveInfo> resolveInfos = pm.queryIntentActivities(intent, 0);
        for (ResolveInfo resolveInfo : resolveInfos) {
            String pkgName = resolveInfo.activityInfo.applicationInfo.packageName;
            if (pkgName.equalsIgnoreCase(context.getPackageName())) {
                String className = resolveInfo.activityInfo.name;
                return className;
            }
        }
        return null;
    }


    public static boolean showNetworkUnavailable(final Context context) {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(context);
        builder.setMessage("Sorry there was an error getting data from the Internet.\nNetwork Unavailable!");
        builder.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (NetworkUtil.getConnectivityStatus(context) == 0) {
                    dialog.dismiss();
                    showNetworkUnavailable(context);
                    now=false;
                } else {
                    now=true;
                    dialog.dismiss();
                }
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
        Toast.makeText(context, "Network Unavailable!", Toast.LENGTH_LONG).show();
        return now;
    }
}
