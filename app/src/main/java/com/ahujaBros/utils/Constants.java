package com.ahujaBros.utils;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by insonix on 25/5/17.
 */

public abstract class Constants {

    public static final String KEY_CATID = "catID";
    public static final String KEY_SUBCATID = "subcatID";
    public static final String KEY_DEPTTID = "departID";
    public static final String KEY_TITLE = "title";
    public static final String KEY_SUBITEMS = "subCatItems";
    public static final String KEY_FRAGTITLE = "title";

    @Retention(RetentionPolicy.SOURCE)
    @IntDef({SORTED, UNSORTED})
    public @interface Sort {
    }

    // Declare the constants
    public static final int SORTED = 1;
    public static final int UNSORTED = 0;

    // Decorate the target methods with the annotation
    @Sort
    public abstract int getSort();

    // Attach the annotation
    public abstract void setSort(@Sort int mode);

}
