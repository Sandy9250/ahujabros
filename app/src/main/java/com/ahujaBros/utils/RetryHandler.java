package com.ahujaBros.utils;

/**
 * Created by insonix on 14/6/17.
 */

public interface RetryHandler {
    boolean shouldRetry(Throwable t, int attemptNumber) ;
}
