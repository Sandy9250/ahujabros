package com.ahujaBros.model.AddToCart;

public class AddToCartResponse{
	private int bagId;
	private String data;
	private String message;
	private String status;

	public void setBagId(int bagId){
		this.bagId = bagId;
	}

	public int getBagId(){
		return bagId;
	}

	public void setData(String data){
		this.data = data;
	}

	public String getData(){
		return data;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"AddToCartResponse{" + 
			"bag_id = '" + bagId + '\'' + 
			",data = '" + data + '\'' + 
			",message = '" + message + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}
