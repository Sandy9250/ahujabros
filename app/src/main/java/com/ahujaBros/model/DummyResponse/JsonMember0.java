package com.ahujaBros.model.DummyResponse;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class JsonMember0{

	@SerializedName("subGroupName")
	private String subGroupName;

	@SerializedName("groupName")
	private String groupName;

	@SerializedName("quantity")
	private String quantity;

	@SerializedName("catName")
	private String catName;

	@SerializedName("product_info")
	private ProductInfo productInfo;

	public void setSubGroupName(String subGroupName){
		this.subGroupName = subGroupName;
	}

	public String getSubGroupName(){
		return subGroupName;
	}

	public void setGroupName(String groupName){
		this.groupName = groupName;
	}

	public String getGroupName(){
		return groupName;
	}

	public void setQuantity(String quantity){
		this.quantity = quantity;
	}

	public String getQuantity(){
		return quantity;
	}

	public void setCatName(String catName){
		this.catName = catName;
	}

	public String getCatName(){
		return catName;
	}

	public void setProductInfo(ProductInfo productInfo){
		this.productInfo = productInfo;
	}

	public ProductInfo getProductInfo(){
		return productInfo;
	}

	@Override
 	public String toString(){
		return 
			"JsonMember0{" + 
			"subGroupName = '" + subGroupName + '\'' + 
			",groupName = '" + groupName + '\'' + 
			",quantity = '" + quantity + '\'' + 
			",catName = '" + catName + '\'' + 
			",product_info = '" + productInfo + '\'' + 
			"}";
		}
}