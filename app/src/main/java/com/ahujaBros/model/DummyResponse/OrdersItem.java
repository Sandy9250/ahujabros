package com.ahujaBros.model.DummyResponse;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class OrdersItem{

	@SerializedName("181")
	private JsonMember181 jsonMember181;

	public void setJsonMember181(JsonMember181 jsonMember181){
		this.jsonMember181 = jsonMember181;
	}

	public JsonMember181 getJsonMember181(){
		return jsonMember181;
	}

	@Override
 	public String toString(){
		return 
			"OrdersItem{" + 
			"181 = '" + jsonMember181 + '\'' + 
			"}";
		}
}