package com.ahujaBros.model.DummyResponse;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class JsonMember181{

	@SerializedName("0")
	private JsonMember0 jsonMember0;

	@SerializedName("date")
	private String date;

	@SerializedName("1")
	private JsonMember1 jsonMember1;

	@SerializedName("2")
	private JsonMember2 jsonMember2;

	@SerializedName("3")
	private JsonMember3 jsonMember3;

	@SerializedName("total_amount")
	private int totalAmount;

	public void setJsonMember0(JsonMember0 jsonMember0){
		this.jsonMember0 = jsonMember0;
	}

	public JsonMember0 getJsonMember0(){
		return jsonMember0;
	}

	public void setDate(String date){
		this.date = date;
	}

	public String getDate(){
		return date;
	}

	public void setJsonMember1(JsonMember1 jsonMember1){
		this.jsonMember1 = jsonMember1;
	}

	public JsonMember1 getJsonMember1(){
		return jsonMember1;
	}

	public void setJsonMember2(JsonMember2 jsonMember2){
		this.jsonMember2 = jsonMember2;
	}

	public JsonMember2 getJsonMember2(){
		return jsonMember2;
	}

	public void setJsonMember3(JsonMember3 jsonMember3){
		this.jsonMember3 = jsonMember3;
	}

	public JsonMember3 getJsonMember3(){
		return jsonMember3;
	}

	public void setTotalAmount(int totalAmount){
		this.totalAmount = totalAmount;
	}

	public int getTotalAmount(){
		return totalAmount;
	}

	@Override
 	public String toString(){
		return 
			"JsonMember181{" + 
			"0 = '" + jsonMember0 + '\'' + 
			",date = '" + date + '\'' + 
			",1 = '" + jsonMember1 + '\'' + 
			",2 = '" + jsonMember2 + '\'' + 
			",3 = '" + jsonMember3 + '\'' + 
			",total_amount = '" + totalAmount + '\'' + 
			"}";
		}
}