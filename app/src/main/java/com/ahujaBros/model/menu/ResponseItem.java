package com.ahujaBros.model.menu;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class ResponseItem{

	@SerializedName("Department")
	private String department;

	@SerializedName("Department_image")
	private String departmentImage;

	@SerializedName("Cat")
	private List<CatItem> cat;

	public void setDepartment(String department){
		this.department = department;
	}

	public String getDepartment(){
		return department;
	}

	public void setDepartmentImage(String departmentImage){
		this.departmentImage = departmentImage;
	}

	public String getDepartmentImage(){
		return departmentImage;
	}

	public void setCat(List<CatItem> cat){
		this.cat = cat;
	}

	public List<CatItem> getCat(){
		return cat;
	}

	@Override
 	public String toString(){
		return 
			"ResponseItem{" + 
			"department = '" + department + '\'' + 
			",department_image = '" + departmentImage + '\'' + 
			",cat = '" + cat + '\'' + 
			"}";
		}
}