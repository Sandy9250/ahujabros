package com.ahujaBros.model.Search;

import java.util.List;
import javax.annotation.Generated;

import com.ahujaBros.model.CategoryDetail.ProductsItem;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class Data{

	@SerializedName("products")
	private List<ProductsItem> products;

	public void setProducts(List<ProductsItem> products){
		this.products = products;
	}

	public List<ProductsItem> getProducts(){
		return products;
	}

	@Override
 	public String toString(){
		return 
			"Data{" + 
			"products = '" + products + '\'' + 
			"}";
		}
}