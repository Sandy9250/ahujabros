package com.ahujaBros.model.home;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class HomeResponse{

	@SerializedName("Status")
	private String status;

	@SerializedName("response")
	private List<ResponseItem> response;

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	public void setResponse(List<ResponseItem> response){
		this.response = response;
	}

	public List<ResponseItem> getResponse(){
		return response;
	}

	@Override
 	public String toString(){
		return 
			"HomeResponse{" + 
			"status = '" + status + '\'' + 
			",response = '" + response + '\'' + 
			"}";
		}
}