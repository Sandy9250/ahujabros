package com.ahujaBros.model.cart;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("com.robohorse.robopojogenerator")
public class MyCart{

	@SerializedName("data")
	private List<DataItem> data;

	@SerializedName("bag_total")
	private int bagTotal;

	@SerializedName("total_payable")
	private int totalPayable;

	@SerializedName("bag_discount")
	private int bagDiscount;

	@SerializedName("total_items")
	private int totalItems;

	@SerializedName("status")
	private String status;

	public void setData(List<DataItem> data){
		this.data = data;
	}

	public List<DataItem> getData(){
		return data;
	}

	public void setBagTotal(int bagTotal){
		this.bagTotal = bagTotal;
	}

	public int getBagTotal(){
		return bagTotal;
	}

	public void setTotalPayable(int totalPayable){
		this.totalPayable = totalPayable;
	}

	public int getTotalPayable(){
		return totalPayable;
	}

	public void setBagDiscount(int bagDiscount){
		this.bagDiscount = bagDiscount;
	}

	public int getBagDiscount(){
		return bagDiscount;
	}

	public void setTotalItems(int totalItems){
		this.totalItems = totalItems;
	}

	public int getTotalItems(){
		return totalItems;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"MyCart{" + 
			"data = '" + data + '\'' + 
			",bag_total = '" + bagTotal + '\'' + 
			",total_payable = '" + totalPayable + '\'' + 
			",bag_discount = '" + bagDiscount + '\'' + 
			",total_items = '" + totalItems + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}